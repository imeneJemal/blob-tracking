import cv2
import imutils
import numpy as np
import image_analysis
def draw_hand_rect(frame):
    rows,cols,_ = frame.shape
    for i in range(size):
        cv2.rectangle(frame,(int(hand_col_nw[i]),int(hand_row_nw[i])),(int(hand_col_se[i]),int(hand_row_se[i])),(0,255,0),1)
    return frame_final
def train_hand(frame):
    # convert the frame to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # Initialize the ROI with zeros
    roi = np.zeros([90,10,3], dtype=hsv.dtype)
    size = 9
    for i in range(size):
        # Put the extracted pixels in the ROI
        roi[i*10:i*10+10,0:10] = hsv[hand_row_nw[i]:hand_row_nw[i]+10, hand_col_nw[i]:hand_col_nw[i]+10]
    # calculate the hist
    #histsize [180, 256]: Quantize the hue to 30 levels,and the saturation to 32 levels
    #ranges:hue varies from 0 to 180, see cvtColor saturation varies from 0 to 255
    hand_his = cv2.calcHist([roi],[0, 1], None, [180, 256], [0, 180, 0, 256])
    # normalize the hist to (0..255) values
    cv2.normalize(hand_his, hand_his, 0, 255, cv2.NORM_MINMAX)
    return hand_his
def draw_final(frame,hand_his):
    hand_masked = image_analysis.apply_hist_mask(frame, hand_his)
    cv2.imshow("hand_masked",hand_masked)
    contours = image_analysis.contours(hand_masked)
    if contours is not None and len(contours) > 0:
        max_contour = max(contours, key=cv2.contourArea)
        cv2.drawContours(frame, max_contour, -1, (255, 0, 0),2)

        hull = cv2.convexHull(max_contour)
        centroid = image_analysis.centroid(max_contour)
        defects = image_analysis.defects(max_contour)

        if centroid is not None and defects is not None and len(defects) > 0:
            farthest_point = image_analysis.farthest_point(defects, max_contour, centroid)
            print (farthest_point)
            if farthest_point is not None:
                image_analysis.plot_farthest_point(frame, farthest_point)
if __name__ == '__main__' :

    # get the camera refrence
    camera = cv2.VideoCapture(1)
    # global varibales
    trained_hand=False
    bbox = (287, 23, 86, 320)
    hand_row_nw = np.array([bbox[1],bbox[1],bbox[1],bbox[1]+40,bbox[1]+40,bbox[1]+40,bbox[1]+80,bbox[1]+80,bbox[1]+80])
    hand_col_nw = np.array([bbox[0],bbox[0]+30,bbox[0]+60,bbox[0],bbox[0]+30,bbox[0]+60,bbox[0],bbox[0]+30,bbox[0]+60])
    hand_row_se = hand_row_nw + 10
    hand_col_se = hand_col_nw + 10
    hand_his = None
    size = hand_row_nw.size

    # keep looping
    while True:
        # get a frame
        (grabbed, frame_in) = camera.read()
        # original frame
        frame_orig = frame_in.copy()
        # resize
        frame = imutils.resize(frame_in)
        # flipped frame to draw on
        frame_final = cv2.flip(frame,1)
        # create frame depending on trained status
        if not trained_hand:
            draw_hand_rect(frame_final)
        else:
            draw_final(frame_final,hand_his)
        # click h to train hand
        if cv2.waitKey(1) == ord('h') & 0xFF:
            if not trained_hand:
                hand_his=train_hand(frame)
                trained_hand=True

        # click q to quit
        if cv2.waitKey(1) == ord('q') & 0xFF:
            break

        # display frame
        cv2.imshow('image', frame_final)
    camera.release()
    cv2.destroyAllWindows()
