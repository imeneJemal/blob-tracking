* Description: In this example we use skin detection(based on color) to segment
  the hand. After that we detect the fingertip using the convexity defect.
* Defenitions:
 -Bitewise:a bitwise operation operates on one or more bit patterns or binary
  numerals at the level of their individual bits.
 -Channel:Channels are different dimensions of an image that holds value for
  every pixel. So for example in case of an
  RGB image we have 3 channels.

* Algorithm:
 -Read a frame from the camera.
 -if hand is not trained:
  -Draw nine rectangles(area where the user should put his hand to detect the
   skin color).
  -Click h to train hand.
  -Train hand
   -Extract pixels from these rectangles and use them to create an HSV
    histogram(hand hist).
   -calculate and normalize the histogram.
 -else(hand is trained):
  -Skin detection
   -Convert the frame to HSV.
   -Apply backprojection.
   -Threshold and bitwise-AND.
  -Fingertip detection
   -Find contours.
   -Get maximum contours.
   -Get convex hull then convex defects(Any deviation of the object from this
    hull can be considered as convexity defect).
   -Find furthest point which is the Fingertip.
    -for all points in defects_point:
     -calculate the distance between this point and the centroid of the contour.
     -get the maximum distance.
   -Plot it.



----------
Refrences:
http://www.benmeline.com/finger-tracking-with-opencv-and-python/
https://docs.opencv.org/3.3.1/dc/df6/tutorial_py_histogram_backprojection.html
