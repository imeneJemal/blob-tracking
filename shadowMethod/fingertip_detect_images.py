import cv2
import numpy as np
import image_analysis
import glob
import argparse
bgSubThreshold = 60 # BackgroundSubtractorMOG2 param
learningRate = 0
window_size=20
avg=0
count=0
d=int(window_size/2)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('filepath', help='String Filepath')
    parser.add_argument('car', help='Character Filepath')
    args = parser.parse_args()
    filepath=args.filepath
    car=args.car
    bgModel = cv2.createBackgroundSubtractorMOG2(0, bgSubThreshold)
    bg=cv2.imread(filepath+'background.jpg')
    # bg = cv2.resize(bg,(800,800))
    fgmask = bgModel.apply(bg,learningRate=learningRate)
    images = glob.glob(filepath+'/'+car+'*')
    if len(images)==0:
        print("There are no images in this filepath")
    for i, fname in enumerate(images):
        image = cv2.imread(images[i])
        # image = cv2.resize(image,(800,800))
        img = image_analysis.removeBG(image,bgModel)
        h,w=img.shape
        ret, thresh = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY)
        shadow =cv2.absdiff(img, thresh)
        cnt=image_analysis.max_contour(thresh)
        if cnt is not None:
            # hull = cv2.convexHull(cnt)
            # cv2.drawContours(image, [hull], 0, (0, 0, 255), 1)
            cv2.drawContours(image, [cnt], 0, (0, 255, 0), 1)
            # find and draw furthest  point
            centroid = image_analysis.centroid(cnt)
            defects = image_analysis.defects(cnt)
            if centroid is not None and defects is not None and len(defects) > 0:
                farthest_point = image_analysis.farthest_point(defects, cnt, centroid)
                if farthest_point is not None:
                    cv2.circle(image,farthest_point,5, [255,0,0], -1)
                    if farthest_point[0]>window_size and farthest_point[0]<w-window_size and farthest_point[1]>window_size and farthest_point[1]<h-window_size:
                        percentage=image_analysis.calculate_shadow(window_size,shadow,farthest_point)
                        count=count+1
                        avg=avg+percentage
                        # Display
                        cv2.rectangle(shadow,(farthest_point[0]-d-1, farthest_point[1]+d+1),(farthest_point[0]+d+1,farthest_point[1]-d-1), (255, 255, 255),1)
                        cv2.rectangle(image,(farthest_point[0]-d-1, farthest_point[1]+d+1),(farthest_point[0]+d+1,farthest_point[1]-d-1), (0, 0, 255), 1)
                        cv2.putText(image, 'shadow: '+str(percentage), (20, 370), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1,(255, 200, 0), 2, cv2.LINE_AA)
                        cv2.imshow('hand', thresh)
                        cv2.imshow("shadow",shadow)
                        cv2.imshow('mask', img)
                        cv2.imshow(fname, image)
                        cv2.waitKey()
        k = cv2.waitKey(10)
        if k == 27:  # press ESC to exit
            break
    if count!=0:
        print("count: ",count)
        print("avg: ",avg/count)
