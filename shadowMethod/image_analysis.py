import cv2
import numpy as np

def apply_hist_mask(frame, hist):
	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
	#apply backprojection
	dst = cv2.calcBackProject([hsv], [0,1], hist, [0,180,0,256], 1)
	# Now convolute with circular disc
	disc = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11,11))
	cv2.filter2D(dst, -1, disc, dst)
	# threshold and binary AND
	ret, thresh = cv2.threshold(dst, 100, 255, 0)
	thresh = cv2.merge((thresh,thresh, thresh))
	cv2.GaussianBlur(dst, (3,3), 0, dst)
	res = cv2.bitwise_and(frame, thresh)
	return res

def contours(frame):
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	ret,thresh = cv2.threshold(gray, 0, 255, 0)
	_,contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	return contours
def max_contour(thresh):
	_,contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
	length = len(contours)
	maxArea = -1
	res=None
	if length > 0:
		for i in range(length):  # find the biggest contour (according to area)
			temp = contours[i]
			area = cv2.contourArea(temp)
			if area > maxArea:
				maxArea = area
				ci = i
		res = contours[ci]
	return res
def defects(contour):
	hull = cv2.convexHull(contour, returnPoints=False)
	if hull is not None and len(hull > 3) and len(contour) > 3:
		defects = cv2.convexityDefects(contour, hull)
		return defects
	else:
		return None

def centroid(contour):
	moments = cv2.moments(contour)
	if moments['m00'] != 0:
		cx = int(moments['m10']/moments['m00'])
		cy = int(moments['m01']/moments['m00'])
		return (cx,cy)
	else:
		return None

def farthest_point(defects, contour, centroid):
	s = defects[:,0][:,0]
	cx, cy = centroid

	x = np.array(contour[s][:,0][:,0], dtype=np.float)
	y = np.array(contour[s][:,0][:,1], dtype=np.float)

	xp = cv2.pow(cv2.subtract(x, cx), 2)
	yp = cv2.pow(cv2.subtract(y, cy), 2)
	dist = cv2.sqrt(cv2.add(xp, yp))

	dist_max_i = np.argmax(dist)

	if dist_max_i < len(s):
		farthest_defect = s[dist_max_i]
		farthest_point = tuple(contour[farthest_defect][0])
		return farthest_point
	else:
		return None
def calculate_shadow(window_size,shadow,farthest_point):
	sh_count=0
	bg_count=0
	# calculate the shadow percentage
	cv2.rectangle(shadow,(farthest_point[0]-int(window_size/2)-1, farthest_point[1]-int(window_size/2)+1),(farthest_point[0]+window_size+1,farthest_point[1]+int(window_size/2)+1), (255, 255, 255),1)
	for neighbors in shadow[farthest_point[1]-int(window_size/2):farthest_point[1]+int(window_size/2),farthest_point[0]-int(window_size/2):farthest_point[0]+int(window_size/2)]:
		for neighbor in neighbors:
			if neighbor==0:
				bg_count=bg_count+1
			else:
				sh_count=sh_count+1
		# print("sh_count:",sh_count)
		# print("bg_count:",bg_count)
	percentage=sh_count/(window_size**2)
	return percentage
def removeBG(image,bgModel):
    fgmask = bgModel.apply(image,learningRate=0)
	# kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    # res = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)
    kernel = np.ones((3, 3), np.uint8)
    fgmask = cv2.erode(fgmask, kernel, iterations=1)
    return fgmask

#  (all finger )
# center = (640 / 2, 480 / 2)
# angle90 = 90
# scale = 1.0
#     if type(defects) != type(None):  # avoid crashing.
#
#     cnt = 0
#     for i in range(defects.shape[0]):  # calculate the angle
#         s, e, f, d = defects[i][0]
#         start = tuple(res[s][0])
#         end = tuple(res[e][0])
#         far = tuple(res[f][0])
#         a = math.sqrt((end[0] - start[0]) ** 2 + (end[1] - start[1]) ** 2)
#         b = math.sqrt((far[0] - start[0]) ** 2 + (far[1] - start[1]) ** 2)
#         c = math.sqrt((end[0] - far[0]) ** 2 + (end[1] - far[1]) ** 2)
#         angle = math.acos((b ** 2 + c ** 2 - a ** 2) / (2 * b * c))  # cosine theorem
#         if angle <= math.pi / 2:  # angle less than 90 degree, treat as fingers
#             cnt += 1
#             cv2.circle(frame, end, 8, [211, 84, 0], -1)

def plot_farthest_point(frame, point):
		cv2.circle(frame, point, 5, [0,0,255], -1)
