# organize imports
import cv2
import imutils
import numpy as np
import image_analysis

# global variables
bg = None
#-------------------------------------------------------------------------------
# Function - To find the running average over the background
#-------------------------------------------------------------------------------
def run_avg(image, aWeight):
    global bg
    # initialize the background
    if bg is None:
        bg = image.copy().astype("float")
        return

    # compute weighted average, accumulate it and update the background
    # alpha regulates the update speed
    cv2.accumulateWeighted(image, bg, aWeight)

#-------------------------------------------------------------------------------
# Function - To segment the region of hand in the image
#-------------------------------------------------------------------------------
def segment(image, threshold=308):
    # find the absolute difference between background and current frame
    diff = cv2.absdiff(bg.astype("uint8"), image)

    # threshold the diff image so that we get the foreground
    thresholded = cv2.threshold(diff,threshold,
                                255,
                                cv2.THRESH_BINARY)[1]

    # get the contours in the thresholded image
    (_, cnts, _) = cv2.findContours(thresholded.copy(),
                                    cv2.RETR_EXTERNAL,
                                    cv2.CHAIN_APPROX_SIMPLE)

    # return None, if no contours detected
    if len(cnts) == 0:
        return
    else:
        # based on contour area, get the maximum contour which is the hand
        segmented = max(cnts, key=cv2.contourArea)
        return (thresholded, segmented)

#-------------------------------------------------------------------------------
# Main function
#-------------------------------------------------------------------------------
if __name__ == "__main__":
    # initialize weight for running average
    aWeight = 0.5
    # get the reference to the webcam
    camera = cv2.VideoCapture(0)
    # region of interest (ROI) coordinates
    top, right, bottom, left = 40, 280, 325, 660
    # initialize num of frames
    num_frames = 0
    fgbg = cv2.createBackgroundSubtractorMOG2()
    # keep looping, until interrupted
    while(True):
        if not camera.isOpened():
            print("video does not exist")
            break
        # get the current frame
        (grabbed, frame) = camera.read()

        # resize the frame
        frame = imutils.resize(frame, width=700)

        # flip the frame so that it is not the mirror view
        frame = cv2.flip(frame, 1)

        # clone the frame
        clone = frame.copy()

        # get the height and width of the frame
        (height, width) = frame.shape[:2]

        # get the ROI
        roi = frame[top:bottom, right:left]
        fgmask = fgbg.apply(roi)
        cv2.imshow("fgmask", fgmask)

        # convert the roi to grayscale
        gray = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
        # blur it minimize the high frequency components in the image
        gray = cv2.GaussianBlur(gray, (7, 7), 0)

        # to get the background, keep looking till a threshold is reached
        # so that our running average model gets calibrated
        if num_frames < 30:
            run_avg(gray, aWeight)
        else:
            # segment the hand region
            hand = segment(gray)

            # check whether hand region is segmented
            if hand is not None:
                # if yes, unpack the thresholded image and
                # segmented region
                (thresholded, segmented) = hand
                cv2.imshow("Thesholded", thresholded)
                #diff = cv2.absdiff(bg.astype("uint8"), image)

                # draw the segmented region and display the frame


                # finding convex hull
                hull = cv2.convexHull(segmented,returnPoints = False)
                moments = cv2.moments(segmented)
                if moments['m00']!=0:
                            cx = int(moments['m10']/moments['m00']) # cx = M10/M00
                            cy = int(moments['m01']/moments['m00']) # cy = M01/M00

                centr=(cx+right,cy+top)
                cv2.circle(clone,centr,5,[0,0,255],2)
                # drawing contours
                cv2.drawContours(clone, [segmented+ (right, top)], 0, (0, 255, 0), 0)
                centroid = image_analysis.centroid(segmented)
                defects = image_analysis.defects(segmented)
                if centroid is not None and defects is not None and len(defects) > 0:
                    farthest_point = image_analysis.farthest_point(defects, segmented, centroid)
                    if farthest_point is not None:
                        print ( (farthest_point[0]+right, farthest_point[1]+top))
                        farthest_point=(farthest_point[0]+right, farthest_point[1]+top)
                        cv2.circle(clone, farthest_point,2, [0,0,255], -1)

        # draw the segmented hand
        cv2.rectangle(clone, (left, top), (right, bottom), (0,255,0), 2)

        # increment the number of frames
        num_frames += 1

        # display the frame with segmented hand
        cv2.imshow("Video Feed", clone)
        cv2.waitKey(500)
        # observe the keypress by the user
        keypress = cv2.waitKey(1) & 0xFF

        # if the user pressed "q", then stop looping
        if keypress == ord("q"):
            break

    # free up memory
    camera.release()
    cv2.destroyAllWindows()
