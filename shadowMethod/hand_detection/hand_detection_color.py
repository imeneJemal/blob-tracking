import cv2
from skin_color_detection import ColorDetection

if __name__ == '__main__':
    camera = cv2.VideoCapture("videos/vid2.mp4")
    bd=ColorDetection()
    while(True):
        if not camera.isOpened():
            print("video does not exist")
            break
        ret, frame = camera.read()
        if not ret:
            camera = cv2.VideoCapture("videos/vid2.mp4")
            ret, frame = camera.read()
        center=bd.color_detection(frame)
        frame=cv2.resize(frame, (600,600))
        cv2.imshow("frame",frame)
        # cv2.waitKey(100)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            cv2.waitKey()
camera.release()
cv2.destroyAllWindows()
