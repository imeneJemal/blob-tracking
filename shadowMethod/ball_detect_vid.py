import cv2
import numpy as np
import image_analysis

# parameters
bgSubThreshold = 30 # BackgroundSubtractorMOG2
learningRate = 0
window_size=20
# variables
isBgCaptured = 0   # bool, whether the background captured
# ROI
top=35
left=60
right=310
buttom=450
sh_percent=0.13 # fixed from experiences(images folder)
vid_path="videos/video_ball.mp4"
if __name__ == '__main__':
    # Camera
    camera = cv2.VideoCapture(vid_path)
    while camera.isOpened():
        ret, frame = camera.read()
        if not ret:
            camera = cv2.VideoCapture(vid_path)
            ret, frame = camera.read()
        if isBgCaptured == 1:  # this part wont run until background captured
            img = image_analysis.removeBG(frame,bgModel)
            h,w=img.shape
            ret, thresh = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY)
            shadow =cv2.absdiff(img, thresh)
            cnt=image_analysis.max_contour(thresh)
            if cnt is not None:
                cv2.drawContours(frame, [cnt], 0, (0, 255, 0),2)
                length = len(cnt)
                if length > 0:
                    lowest_point=cnt[0][0]
                    for i in range(length):
                        point=cnt[i][0]
                        if lowest_point[1]<point[1]:
                            lowest_point= point
                    if lowest_point is not None:
                        cv2.circle(frame,(lowest_point[0],lowest_point[1]),2, [255,0,0], -1)
                        if lowest_point[0]>window_size and lowest_point[0]<w-window_size and lowest_point[1]>window_size and lowest_point[1]<h-window_size:
                            percentage=image_analysis.calculate_shadow(window_size,shadow,lowest_point)
                            if percentage<sh_percent:
                                cv2.circle(frame,(lowest_point[0],lowest_point[1]),5, [0,0,255], -1)
                            # # Display
                            cv2.rectangle(frame,(lowest_point[0]-int(window_size/2)-1, lowest_point[1]-int(window_size/2)+1),(lowest_point[0]+window_size+1,lowest_point[1]+int(window_size/2)+1), (255, 255, 255),1)

                            cv2.putText(frame, 'shadow: '+str(percentage), (20, 370), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1,(255, 200, 0), 2, cv2.LINE_AA)
                            cv2.imshow('hand', thresh)
                            cv2.imshow("shadow",shadow)
                            cv2.imshow('mask', img)
                            cv2.waitKey(300)  # delay to see the video slower

        cv2.imshow("frame", frame)
        k = cv2.waitKey(10)
        if k == 27:  # press ESC to exit
            break
        elif k == ord('b'):  # press 'b' to capture the background
            bgModel = cv2.createBackgroundSubtractorMOG2(1, bgSubThreshold)
            isBgCaptured = 1
            print( '!!!Background Captured!!!')
        elif k == ord('r'):  # press 'r' to reset the background
            bgModel = None
            isBgCaptured = 0
            print ('!!!Reset BackGround!!!')
