## Description:  Touch tracking on any surface
This project aim to turn an ordinary surface into a touch screen using camera and projector.
## Method
We used the shadow analysis[1] method which is based on calculating the
percentage of shadow into a small window on the neighborhood of the fingertip.
To get this result we followed this steps:
* We used background subtraction to extract the hand and the shadow (BackgroundSubtractorMOG2 function in openCV)
* Now to get the hand contour we use the function "findContours", we search for the biggest contour which our hand (we assume that there is only our hand in the frame)
* After picking up our hand, we create its hull,detects[3] and find the farthest point which is the fingertip.
* After getting the fingertip position, we calculate the number of pixels of shadow in a 20*20 window on the neighborhood of the fingertip. if the percentage of the shadow is up to a threshold value we consider it as a touch. The threshold value is fixed experimentally.
## Source Code Overview
* fingertip_detect_images.py: takes tow argument :
  * filepath: where the images are saved
  * car: character 't' or 'n', to Select images while touching the surface or images while not touching the surface
  this file is used to determined the threshold value of the shadow percentage.
* fingertip_detect_video.py: takes one argument which is the video path
  run this file then press 'b' to capture from the video the background model (Remember to take the Background without hand).
  In the video will appear a red circle on the fingertip when there is a touch
* fingertip_detect_cam.py: same as fingertip_detect_video but it use a webcam to get the background
  press 'b' to capture the background
  press 'r' to reset the background model
  press 'ESC' to exit
## Issues
* The results are not accurate
* There is no synchronization between the camera and the projector  
## Perspective
* Use other methods to extract the hand(there are some exp in the hand_detection folder)
  * skin color method
  * background subtraction without openCV
  * back projection[4]
*   Other methods to extract the shadow
  * threshold the image[1]
* Select other neighborhood of the fingertip
## References
[1] https://jsthomas.github.io/docs/vkeyboard/vkeyboard.pdf
[2] https://docs.opencv.org/3.1.0/db/d5c/tutorial_py_bg_subtraction.html
http://layer0.authentise.com/segment-background-using-computer-vision.html
[3] https://github.com/lzane/Fingers-Detection-using-OpenCV-and-Python
[4] https://docs.opencv.org/3.4/da/d7f/tutorial_back_projection.html
