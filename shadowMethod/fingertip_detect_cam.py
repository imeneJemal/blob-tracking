import cv2
import numpy as np
import image_analysis
# parameters
bgSubThreshold = 30 # BackgroundSubtractorMOG2
learningRate = 0
window_size=20
# variables
isBgCaptured = 0   # bool, whether the background captured
# ROI
top=35
left=60
right=310
buttom=450
sh_percent=0.08 # fixed from experiences(images)
# Camera
camera = cv2.VideoCapture(1)
# camera.set(10,200)
while camera.isOpened():
    ret, frame = camera.read()
    # frame = cv2.bilateralFilter(frame, 5, 50, 100)  # smoothing filter
    # frame = cv2.flip(frame, 0)  # flip the frame vertically
    # frame = cv2.flip(frame, 1)  # flip the frame horizontally
    bg =cv2.imread("bg.jpg")
    bg = cv2.resize(bg,(640,480))
    if isBgCaptured == 1:  # this part wont run until background captured
        img=frame[top:buttom,left:right]  # clip the ROI
        bg0=bg[top:buttom,left:right]  # clip the ROI
        img = image_analysis.removeBG(img,bgModel)
        # img = img[top:buttom,left:right]  # clip the ROI
        ret, thresh = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY)
        # cv2.imshow('hand', thresh)
        shadow =cv2.absdiff(img, thresh)
        h,w=shadow.shape
        cnt=image_analysis.max_contour(thresh)
        if cnt is not None:
            cv2.drawContours(bg, [cnt], 0, (0, 255, 0),2)
            #find and draw furthest  point
            centroid = image_analysis.centroid(cnt)
            defects = image_analysis.defects(cnt)
            if centroid is not None and defects is not None and len(defects) > 0:
                farthest_point = image_analysis.farthest_point(defects, cnt, centroid)
                if farthest_point is not None:
                    if farthest_point[0]>window_size and farthest_point[0]<w-window_size and farthest_point[1]>window_size and farthest_point[1]<h-window_size:
                        percentage=image_analysis.calculate_shadow(window_size,shadow,farthest_point)
                        if percentage<sh_percent:
                            cv2.circle(bg0,(farthest_point[0],farthest_point[1]),5, [0,0,255], -1)
                        # Display
                        cv2.putText(bg0, 'shadow: '+str(percentage), (20, 370), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1,(255, 200, 0), 2, cv2.LINE_AA)

        bg[top:buttom,left:right]=bg0  # clip the ROI

    cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("window",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
    cv2.imshow("window", bg)

    k = cv2.waitKey(10)
    if k == 27:  # press ESC to exit
        break
    elif k == ord('b'):  # press 'b' to capture the background
        bgModel = cv2.createBackgroundSubtractorMOG2(1, bgSubThreshold)
        isBgCaptured = 1
        print( '!!!Background Captured!!!')
    elif k == ord('r'):  # press 'r' to reset the background
        bgModel = None
        isBgCaptured = 0
        print ('!!!Reset BackGround!!!')
