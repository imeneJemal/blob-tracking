import cv2
import numpy as np

cam = cv2.VideoCapture(1)
i=0
while(True):
    if not (cam.grab()):
        print("No more frames")
        break
    _, frame = cam.read()
    name='images3/not_touching'+str(i)+'.jpg'
    cv2.imshow("frame",frame)
    if cv2.waitKey(1) & 0xFF == ord('c'):
        cv2.imwrite(name, frame)
        i += 1
cam.release()
cv2.destroyAllWindows()
