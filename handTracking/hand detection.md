* Description: this is an example of hand_gesture_recognition.
 It is divided in tow parts:
 -Part1(hand_detection): hand segmentation using background subtraction and
  motion detection, it will be detailed below.
 -Part2(hand_gesture_recognition):a method to count fingers, but it is not
  so effective.

* Definitions:
 -Background subtraction: Give the system frames for the background to get
  background model(using running average in this example).
  for any new frame it calculate the absolute difference between the background
  model and the current frame to obtain a difference image contains the new object.
 -Running average:is an average that continually changes as more data points
  are collected.
 -Motion detection: is the process of detecting a change in the position of an
  object relative to its surroundings.To do that we threshold the result image
  of the background subtraction method, so that the object becomes visible and
  all other regions are painted as black.
 -Gaussian Blur(Gaussian smoothing): is a filter to reduce image noise and
  reduce detail.

* Algorithm(hand_detection):
 -Read, resize(to minimize processed area),flip(avoid mirror view) the image
  from the video.
 -Convert image to gray-scale and blur it(filter the image)
 -Get 30 frames of the background to calculate the background model using
  running average.
 -Segment the hand region.
  -find the absolute difference between background and current frame.
  -threshold the result image.
  -get the contours in the thresholded image.
  -get the maximum contour which is the hand.
 -Draw contour and display result.

 * Rq: The algorithm fails if there is a larger object than the hand inside
  this frame.

----------
Reference :
https://gogul09.github.io/software/hand-gesture-recognition-p1
https://gogul09.github.io/software/hand-gesture-recognition-p2
