*Description:We have followed the methodology proposed by Malima et al.

*Steps:
 -Convex hull+ Extreme points:Find the convex hull of the segmented hand region
  (which is a contour) and compute the most extreme points in the convex hull
  (Extreme Top, Extreme Bottom, Extreme Left, Extreme Right).
 -Finding the center of palm: Find the center of palm using these extremes
  points in the convex hull.
 -Construct a circle with palm's center and around the fingers:Using the palm’s
  center, construct a circle with the maximum Euclidean distance
  (between the palm’s center and the extreme points) as radius.
 -bitwise-AND between hand region and circle (mask): Perform bitwise-AND
  operation between the thresholded hand image (frame) and the circular ROI
  (mask). This reveals the finger slices, which could further be used to
  calculate the number of fingers shown.
