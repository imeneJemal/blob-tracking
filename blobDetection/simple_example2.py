import cv2
import imutils
import numpy as np

camera = cv2.VideoCapture(0)
top, right, bottom, left = 10, 350, 225, 590
# Set parameters
params = cv2.SimpleBlobDetector_Params()

# Change thresholds
params.minThreshold = 0;
params.maxThreshold = 255;

# Filter by Area.
params.filterByArea = True
params.minArea = 500
params.maxArea = 2000
# Filter by Circularity
params.filterByCircularity = False
params.minCircularity = 0.85

#Filter by Convexity
params.filterByConvexity = False
params.minConvexity =0.87
# Filter by Inertia
params.filterByInertia = False
params.minInertiaRatio = 0.01
while(True):
    # Capture frame-by-frame
    ret, frame = camera.read()

    # Our operations on the frame come here
    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # clone the frame
    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #clone the frame
    clone = frame.copy()
    #Set up the detector with default parameters.
    detector = cv2.SimpleBlobDetector_create(params)

    # Detect blobs.
    keypoints = detector.detect(clone)

    # Draw detected blobs as red circles.
    # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle
    # corresponds to the size of blob
    im_with_keypoints = cv2.drawKeypoints(clone, keypoints, np.array([]),
    (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    # Display the resulting frame
    cv2.imshow('Video',im_with_keypoints)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
