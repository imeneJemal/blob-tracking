# Standard imports
import cv2
import numpy as np;

# Read image
im = cv2.imread("images/im_with_keypoints.jpg", cv2.IMREAD_GRAYSCALE)

# Set parameters to the the detector
params = cv2.SimpleBlobDetector_Params()

# Change thresholds
params.minThreshold = 0;
params.maxThreshold = 255;

# Filter by Area.
params.filterByArea = True
params.minArea = 500
params.maxArea = 5000

# Filter by Circularity
params.filterByCircularity = True
params.minCircularity = 0.6

#Filter by Convexity
params.filterByConvexity = False
params.minConvexity =0.87
# Filter by Inertia
params.filterByInertia = False
params.minInertiaRatio = 0.01

#Set up the detector with parameters.
detector = cv2.SimpleBlobDetector_create(params)

# Detect blobs.
keypoints = detector.detect(im)

# Draw detected blobs as red circles.
# cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the
# circle corresponds to the size of blob
im_with_keypoints = cv2.drawKeypoints(im, keypoints, np.array([]), (0,0,255),
cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

# Show keypoints
cv2.imshow("Keypoints", im_with_keypoints)
cv2.imwrite("images/im_with_keypoints.jpg",im_with_keypoints)
# show the image for 10000ms
cv2.waitKey(10000)
