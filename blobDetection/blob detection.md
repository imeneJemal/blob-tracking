* Description: this example is a simple introduction to blob detection.
  We are going to use the most popular library which is openCv.
  In simple_example1 we apply the algo on images.
  In simple_example2 we apply the algo on video using the web-cam.

* Definitions:
  -Computer vision: is a field of Artificial Intelligence,it is mainly aimed at
   real-time image processing and analyzing.
  -OpenCv (Open computer vision): it is an open source graphic library,for
   computer vision.
  -Blob: is a group of connected pixels in an image that share
   some common property ( E.g grayscale value ).
  -Detection: object detection is scanning and searching for an object.
  -Tracking: track how an object is moving, where is it going, or its speed.
   There is tow methods of tracking:
    -Dense method: tracking using series of detection. it called dense because
     we are processing every pixel.
    -Sparse method:detect the position at some time 't', we try to estimate the
     position at 't+dt'.we detect the position at 't+dt' using the estimation and
     the image a 't+dt'.It called a sparse method because we are processing only
     pixels near to the area where we are estimating object to be.
  -Thresholding an image: is the simplest method of image segmentation.
   Can be applied to get a binary image(0,1) from a grayscale image(0,255):
   Replace values grater than a fixed constant by 1(white), values less than the fixed
   values by 0(black).

* Algorithm:
  -Read image in gray-scale
  -Use the Simple_blob_detector of openCV (algo based on the following steps)
    -Thresholding : Convert the source images to several binary images by
     thresholding(starting from minThreshold to maxThreshold by thresholdStep).
    -Grouping : In each binary image,  connected white pixels(blobs) are
     grouped together.
    -Merging : Compute the centers of binary blobs,then merge the blobs that
     are located closer than minDistBetweenBlobs.
    -Center & Radius Calculation : Compute the center of the new merged blobs
  -Draw the circles and Display the result.

* OpenCv methods:
  -SimpleBlobDetector_create: detect blobs and take parameters to filter the
   results:
    -min and max thresholds to change thresholding.
    -parameters to filter by Area, Circularity, Convexity,Inertia.(1)
  -drawKeypoints: (image,keypoints,outImage,color,flags)
    -outImage depends on flags.
    -possible value of flags (DRAW_MATCHES_FLAGS+):DEFAULT, DRAW_OVER_OUTIMG,
      NOT_DRAW_SINGLE_POINTS, DRAW_RICH_KEYPOINTS (4)

----------
Reference :
1) https://www.learnopencv.com/blob-detection-using-opencv-python-c/
2) https://docs.opencv.org/3.3.1/d0/d7a/classcv_1_1SimpleBlobDetector.html
3) https://docs.opencv.org/3.4/d7/d4d/tutorial_py_thresholding.html
4) https://docs.opencv.org/3.0-beta/modules/features2d/doc/drawing_
   function_of_keypoints_and_matches.html
