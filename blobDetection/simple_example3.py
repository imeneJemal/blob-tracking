import cv2
import numpy as np;
import imutils

def blobdetect(img):
    frame = imutils.resize(img, width=600)

    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    greenMin=(65,60,60)
    greenMax=(80,255,255)

    green=cv2.inRange(hsv,greenMin,greenMax)
    cv2.imshow("green", green)
    params = cv2.SimpleBlobDetector_Params()
    params.filterByArea = True
    params.minArea=150
    detector=cv2.SimpleBlobDetector_create(params)
    keypoints=detector.detect(255-green)
    im_with_keypoints = cv2.drawKeypoints(img, keypoints, np.array([]), (0,0,255),
    cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    # Show keypoints
    cv2.imshow("Keypoints", im_with_keypoints)
    cv2.waitKey(10000)
    wood=[]
    for i in keypoints:
        x=i.pt[0]; y=i.pt[1]
        wood.append([x,y])

    return wood
img = cv2.imread("images/green-palette.jpg")
a=blobdetect(img)
print(a)
