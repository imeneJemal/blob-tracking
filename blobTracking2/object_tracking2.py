import cv2

if __name__ == '__main__' :

    # Read video
    camera = cv2.VideoCapture(0)

    # Read first frame.
    grabbed, frame = camera.read()
    if not grabbed:
        print ('Cannot connect to the camera!')
        sys.exit()

    # Define an initial bounding box
    bbox = (287, 23, 86, 320)

    # Uncomment the line below to select a different bounding box
    bbox = cv2.selectROI(frame, False)
    tracker1 = cv2.TrackerBoosting_create()
    tracker_type1="Boosting"
    color1=(0, 0, 255)
    tracker2 = cv2.TrackerTLD_create()
    tracker_type2="TDL"
    color2=(255, 0, 191)

    # Initialize tracker with first frame and bounding box
    grabbe1 = tracker1.init(frame, bbox)
    grabbed2 = tracker2.init(frame, bbox)

    while True:
        # Read a new frame
        grabbed, frame = camera.read()

        if not grabbed:
            break

        # Update tracker
        ok1, bbox1 = tracker1.update(frame)
        ok2, bbox2 = tracker2.update(frame)

        # Draw bounding box
        if ok1:
            # Tracking success
            p1 = (int(bbox1[0]), int(bbox1[1]))
            p2 = (int(bbox1[0] + bbox1[2]), int(bbox1[1] + bbox1[3]))
            cv2.rectangle(frame, p1, p2, color1, 2, 1)
        else :
            # Tracking failure
            cv2.putText(frame, "Tracking failure detected", (100,80), cv2.FONT_HERSHEY_SIMPLEX, 0.75,color1,2)

        # Display tracker type on frame
        cv2.putText(frame, tracker_type1 + " Tracker", (100,20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, color1,2);

        # Draw bounding box
        if ok2:
            # Tracking success
            p1 = (int(bbox2[0]), int(bbox2[1]))
            p2 = (int(bbox2[0] + bbox2[2]), int(bbox2[1] + bbox2[3]))
            cv2.rectangle(frame, p1, p2, color2, 2, 1)
        else :
            # Tracking failure
            cv2.putText(frame, "Tracking failure detected", (100,80), cv2.FONT_HERSHEY_SIMPLEX, 0.75,color2,2)

        # Display tracker type on frame
        cv2.putText(frame, tracker_type2 + " Tracker", (100,40), cv2.FONT_HERSHEY_SIMPLEX, 0.75, color2,2);

        # Display result
        cv2.imshow("Tracking", frame)

        # Exit if ESC pressed
        k = cv2.waitKey(1) & 0xff
        if k == 27 : break
