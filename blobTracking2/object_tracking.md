* Description: In this project we are going to talk about different algorithm
  of object tracking.The first example(object tracking1) is a test each
  tracking algorithm. In the second one(objects tracking2) we are testing to
  algorithm to compare the results.

* Definitions:

 -Detection: object detection is scanning and searching for an object.

 -Tracking: track how an object is moving, where is it going, or its speed in
  the current frame given we have tracked the object successfully in previous
  frames.

 -Motion model: speed and direction of motion of the object

 -How tracking is done?
  -Specify the object you want to track.
  -Build the motion model that predicts the approximate location of the object.
  -Build an appearance model that encodes what the object looks like.
   This model can be used to search in a small neighborhood of the of the object.
  -This appearance model is a classifier that is trained in an on-line
   manner(The algo is trained at run time).
  -The job of the classifier is to classify a rectangular region of an image as
   either an object or background.GOTURN
  -The classifier is trained by feeding it positive (object) and negative
   (background) examples.Then It take an input
   image and return a score 0:background, 1:object.

 -Detection vs Tracking:
  -While detecting we analyze all the pixels of the image,While tracking we
   analyze a region of pixels.
  -Tracking is faster then detection.
  -Tracking can help when detection fails.
  -Detection is necessary when tracking fails.
  -Tracking preserves identity (we track a specific object).
  -Detection algorithms are trained on a large number of examples of the object.
   They, therefore, have more knowledge about the general class of the object.
  -Tracking algorithms know more about the specific
   instance of the class they are tracking.

 -Existing tracking algo:
  1) BOOSTING Tracker:
    -Description: Based on an on-line version of AdaBoost algorithm.
      The classifier need to be trained at runtime.
     -The user supply a box which is taken as the positive example and many
      image patches outside the bounding
      box are treated as the background.
     -On the new frame,the algo is run in every pixel on neighborhood of the
      previous location and the score of the classifier is recorded.
     -The new location of the object is the one where the score is maximum.
     -The classifier is updated with this additional data.
    -Advantages:
     -It works! :p
    -Disadvantages:
     -Tracking performance is mediocre.
     -It does not reliably know when tracking has failed.
  2) MIL (Multiple Instance Learning) Tracker:
    -Description: Same idea as BOOSTING tracker but instead of only taking the
     current location of the object as positive instance, it looks in a small
     neighborhood around to generate several potential positive examples called
     positive bags.
     In MIL, we do not specify positive and negative examples, but positive
     and negative “bags”.
    -Advantages:
     -The performance is pretty good.
     -It does a reasonable job under partial occlusion.
    -Disadvantages:
     -Tracking failure is not reported reliably.
     -Does not recover from full occlusion.
  3) KCF (Kernelized Correlation Filters) Tracker:
    -Description:This tracker builds on the ideas presented in the previous two
     trackers.
     This tracker utilizes that fact that the multiple positive samples used in
     the MIL tracker have large overlapping regions.
     This overlapping data leads to some nice mathematical properties that is
     exploited by this tracker to make tracking faster and more accurate at
     the same time.
    -Advantages:
     -Accuracy and speed are both better than MIL.
     -reports tracking failure better than BOOSTING and MIL.
    -Disadvantages:
     -Does not recover from full occlusion.
     -Not implemented in OpenCV 3.0.
  4) TLD (Tracking, learning and detection) Tracker:
     -Description:this tracker decomposes the long term tracking task into three
      component tracking, learning, and detection.
      -The tracker follows the object from frame to frame.
      -The detector localizes all appearances that have been observed so far
       and corrects the tracker if necessary.
      -The learning estimates detector’s errors and updates it to avoid these
       errors in the future
     -Advantages:
      -Works the best under occlusion over multiple frames.
      -Tracks best over scale changes.
     -Disadvantages :
      -Lots of false positives making it almost unusable.
  5) MEDIANFLOW Tracker:
     -Description: this tracker tracks the object in both forward and backward
      directions in time and measures the discrepancies between these two
      trajectories.
      Minimizing this ForwardBackward error enables them to reliably detect
      tracking failures and select reliable trajectories in video sequences.
     -Advantages:
      -Excellent tracking failure reporting
      -Works very well when the motion is predictable and there is no occlusion.
     -Disadvantages:
      -Fails under large motion.
  6) GOTURN tracker:
     -Description: this is the only one based on Convolutional
      Neural Network (CNN). It is also the only one that uses an off-line
      trained model, because of which it is faster that other trackers.
     -Advantages:
      -Robust to viewpoint changes, lighting changes, and deformations.
     -Disadvantages:
      -Does not handle occlusion very well.
      -Bug Alert :there is a bug in the OpenCV 3.2 implementation
       that crashes the program when GOTURN is used.
  -Rq:
     -We can lose track of an object when they go behind an obstacle for
      an extended period of time(occlusion) or if they move so fast that the
      tracking algorithm cannot catch. To fix these problems with tracking
      algorithms, a detection algorithm is run every period.

* Algorithm:
 -Create the tracker.
 -Get frame from the camera.
 -Select region of interest.
 -Start timer.
 -Update tracker.
 -Calculate frame par second.
 -If the tracking is done, draw it on the frame
  else write "tracking failed!".
 -Display frame per second.
 -Display the result.

* Function:
 -GetTickFrequency:returns the number of ticks per second.
 -This code computes the execution time in seconds:
	double t = (double)getTickCount();
	// do something ...
	t = ((double)getTickCount() - t)/getTickFrequency();

----------
Refrences:
https://www.learnopencv.com/object-tracking-using-opencv-cpp-python/
https://docs.opencv.org/3.0-beta/modules/tracking/doc/tracker_algorithms.html
https://docs.opencv.org/3.3.1/d7/d4c/classcv_1_1TrackerGOTURN.html
https://docs.opencv.org/3.4/dc/d71/tutorial_py_optimization.html
