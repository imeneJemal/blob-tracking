import numpy as np
import cv2
import image_analysis

vid_path='intro.mp4'
red_img_path="images/red.png"
blue_img_path="images/blue.png"
yellow_img_path="images/yellow.png"
orange_img_path="images/orange.png"
image_path="images/image.png"
lower = np.array([0,30,60], dtype = "uint8")
upper = np.array([20,150,255], dtype = "uint8")
cap = cv2.VideoCapture(vid_path)
camera = cv2.VideoCapture(1)
c=0
r,o,j,b=False,False,False,False
nbr=40 #frame number to select the region (hovring with hand in 40 frame will change the image )
while(cap.isOpened()) and(camera.isOpened()):
    ret1, frame1 = camera.read()
    ret2, frame2 = cap.read()

    cv2.namedWindow("Final", cv2.WINDOW_FREERATIO)
    cv2.setWindowProperty("Final",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
    if ret1:
        if ret2:
            height,width,_=frame1.shape
            image = cv2.imread(image_path,-1)
            image= cv2.resize(image,(width,height), interpolation = cv2.INTER_AREA)
            converted = cv2.cvtColor(frame1, cv2.COLOR_BGR2HSV)
            skinMask = cv2.inRange(converted, lower, upper)

        	# apply a series of erosions and dilations to the mask
        	# using an elliptical kernel
            kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11))
            skinMask = cv2.erode(skinMask, kernel, iterations = 2)
            skinMask = cv2.dilate(skinMask, kernel, iterations = 2)

        	# blur the mask to help remove noise, then apply the
        	# mask to the frame
            skinMask = cv2.GaussianBlur(skinMask, (3, 3), 0)
            skin = cv2.bitwise_and(frame1, frame1, mask = skinMask)

            contours = image_analysis.contours(skin)
            if contours is not None and len(contours) > 0:
                max_contour = max(contours, key=cv2.contourArea)
                cv2.drawContours(image, max_contour, -1, (0, 0, 0),2)
                cv2.drawContours(skin, max_contour, -1, (255, 0, 0),2)
                centroid = image_analysis.centroid(max_contour)
                cv2.circle(image, centroid, 5, [0,0,255], -1)
                image = cv2.flip(image, 1)
                cv2.imshow("Final", image)
                if centroid[0]>0 and centroid[0]<width/2: # region red and yellow
                    if centroid[1]>0 and centroid[1]<height/2:# region red
                        if r==True:
                            c=c+1
                        else:
                            r=True
                            o=False
                            b=False
                            j=False
                            c=0
                    else: # region yellow
                        if j:
                            c=c+1
                        else:
                            j=True
                            o=False
                            b=False
                            r=False
                            c=0
                else: # region blue and orange
                    if centroid[1]>0 and centroid[1]<height/2: # region orange
                        if o:
                            c=c+1
                        else:
                            o=True
                            j=False
                            b=False
                            r=False
                            c=0
                    else: # region blue
                        if b:
                            c=c+1
                        else:
                            b=True
                            j=False
                            o=False
                            r=False
                            c=0

                if c>nbr:
                    if r:
                        image_selected = cv2.imread(red_img_path,-1)
                    if j:
                        image_selected = cv2.imread(yellow_img_path,-1)
                    if o:
                        image_selected = cv2.imread(orange_img_path,-1)
                    if b:
                        image_selected = cv2.imread(blue_img_path,-1)
                    image_selected= cv2.resize(image_selected,(width,height), interpolation = cv2.INTER_AREA)
                    image_selected = cv2.flip(image_selected, 1)
                    cv2.imshow("Final",image_selected)

            else:
                frame2= cv2.resize(frame2,(width,height), interpolation = cv2.INTER_AREA)
                frame2 = cv2.flip(frame2, 1)
                cv2.imshow("Final", frame2)
                b=False
                j=False
                o=False
                r=False
                c=0
            # show the skin in the image along with the mask
            cv2.imshow("images", np.hstack([frame1, skin]))
        else:
           print('video is repeated')
           cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
    else:
        print  ('no camera detetcted')

    key = cv2.waitKey(1) & 0xFF
	# if the 'q' key is pressed, stop the loop
    if key == ord("q"):
        break

cap.release()
cv2.destroyAllWindows()
