# import the necessary packages
from collections import deque
import numpy as np
import imutils
import cv2
import random
# define the lower and upper boundaries of the "green"
# ball in the HSV color space
# greenLower = (65,60,60)
# greenUpper = (80,255,255)
greenLower = (20,60,90)
greenUpper = (130,255,255)
buffer=32
pts = deque(maxlen=buffer)

camera = cv2.VideoCapture(0)

# load fish image
image = cv2.imread("images/fish.png",-1)
background = cv2.imread("images/background.png",-1)
image_mask = image[:,:,3]
image_mask_inv = cv2.bitwise_not(image_mask)
image = image[:,:,0:3]
random_x = random.randint(10, 550)
random_y = random.randint(10, 400)
img_size=50;
# resizing
image = cv2.resize(image,(img_size,img_size),interpolation=cv2.INTER_AREA)
image_mask = cv2.resize(image_mask,(img_size,img_size),interpolation=cv2.INTER_AREA)
image_mask_inv = cv2.resize(image_mask_inv,(img_size,img_size),interpolation=cv2.INTER_AREA)
score=0
top, right, bottom, left = 10,10, 440, 590

# keep looping
while True:
	# grab the current frame
	(grabbed, frame) = camera.read()
	# resize the frame,
	frame = imutils.resize(frame, width=600)
	background = cv2.imread("images/background.png",-1)
	background=imutils.resize(background, width=600)
	frame = cv2.flip(frame, 1)
	# convert it to the HSV color space
	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
	#cv2.imshow("hsv", hsv)

	# construct a mask for the color "green", then perform
	# a series of dilations and erosions to remove any small
	# blobs left in the mask
	mask = cv2.inRange(hsv, greenLower, greenUpper)
	mask = cv2.erode(mask, None, iterations=2)
	mask = cv2.dilate(mask, None, iterations=2)
	#cv2.imshow("mask", mask)
	# find contours in the mask and initialize the current
	# (x, y) center of the ball
	cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)[-2]
	center = None
	x_center=0
	y_center=0
	# only proceed if at least one contour was found
	if len(cnts) > 0:
		# find the largest contour in the mask, then use
		# it to compute the minimum enclosing circle and
		# centroid
		c = max(cnts, key=cv2.contourArea)
		((x_center, y_center), radius) = cv2.minEnclosingCircle(c)
		M = cv2.moments(c)
		center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

		# only proceed if the radius meets a minimum size
		if radius > 10:
			# draw the circle and centroid on the frame,
			# then update the list of tracked points
			cv2.circle(background, center, 5, (0, 0, 255), -1)

		# update the points queue
		pts.appendleft(center)
	#loop over the set of tracked points
	for i in range(1, len(pts)):
		# if either of the tracked points are None, ignore
		# them
		if pts[i - 1] is None or pts[i] is None:
			continue
		# otherwise, compute the thickness of the line and
		# draw the connecting lines
		thickness = int(np.sqrt(buffer / float(i + 1)) * 2.5)
		cv2.line(background, pts[i - 1], pts[i], (0, 0, 255),thickness)


	# add fish image
	roi = background[random_y:random_y+img_size, random_x:random_x+img_size]
	img_bg = cv2.bitwise_and(roi, roi, mask=image_mask_inv)
	img_fg = cv2.bitwise_and(image, image, mask=image_mask)
	dst = cv2.add(img_bg, img_fg)
	background[random_y:random_y + img_size, random_x:random_x + img_size] = dst

	if(x_center!=0 or y_center!=0):
		if (x_center<right+15 or x_center>left-15 or y_center<top+15 or y_center>bottom-15 ):
			cv2.putText(background, str("Game Over!"), (170, 190), cv2.FONT_HERSHEY_COMPLEX_SMALL, 2, (47, 154, 242), 3, cv2.LINE_AA)
			cv2.putText(background, str("Final Score "+str(score)), (210, 230), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (47, 154, 242), 2, cv2.LINE_AA)
			cv2.putText(background, str("Press any key to Exit."), (170, 270), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 200, 0), 2, cv2.LINE_AA)
			break
	if  (x_center>random_x and x_center<(random_x+img_size) and y_center>random_y and y_center<(random_y+img_size)):
		score +=1
		random_x = random.randint(60, 490)
		random_y = random.randint(60, 340)

	#put score
	cv2.putText(background, str("Score - "+str(score)), (250, 400), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 0, 0), 2, cv2.LINE_AA)
	# show the frame to our screen
	cv2.rectangle(background, (left, top), (right, bottom), (0,0,0), 30)
	cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)
	cv2.setWindowProperty("window",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
	cv2.imshow("window", background)
	key = cv2.waitKey(1) & 0xFF


	# if the 'q' key is pressed, stop the loop
	if key == ord("q"):
		break
cv2.namedWindow("window end", cv2.WND_PROP_FULLSCREEN)
cv2.setWindowProperty("window end",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
cv2.imshow("window end", background)
key = cv2.waitKey(0)
# cleanup the camera and close any open windows
camera.release()
cv2.destroyAllWindows()
