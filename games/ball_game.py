# import the necessary packages
import numpy as np
import imutils
import cv2
import random
#import screeninfo

class Ball:
    def __init__(self,x,y):
        self.x = x
        self.y = y
        self.width = 0
        self.height = 0
        self.speed = (0,1)
        self.active = True

    def getDimensions(self):
        return (self.x, self.y, self.width, self.height)

    def centerOrigin(self):
        return (self.x - self.width/2, self.y - self.height/2)

    def update(self):
        self.x += self.speed[0]
        self.y += self.speed[1]

def create_balls(count):
    balls = list()
    for i in range(count):
        tgt = Ball(random.randint(0,width-imgSize) , 0)
        balls.append(tgt)
    return balls

def hit_value(image, target):
    x,y,_,_=target.getDimensions()
    roi = image[y:y+imgSize, x:x+imgSize]
    roi = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
    thresholded = cv2.threshold(roi, 10, 255, cv2.THRESH_BINARY)[1]
    return cv2.countNonZero(thresholded)

imgSize =80
nBall = 5
score= 0
initialDelay =30
previous=None
width=1000
nFall=0
# screen_id=1
# screen = screeninfo.get_monitors()[screen_id]
# screen_res= screen.width, screen.height

# load ball image
ball = cv2.imread("images/icon.png",-1)
ball_mask = ball[:,:,3]
ball_mask_inv = cv2.bitwise_not(ball_mask)
ball = ball[:,:,0:3]
ball = cv2.resize(ball,(imgSize,imgSize),interpolation=cv2.INTER_AREA)
ball_mask = cv2.resize(ball_mask,(imgSize,imgSize),interpolation=cv2.INTER_AREA)
ball_mask_inv = cv2.resize(ball_mask_inv,(imgSize,imgSize),interpolation=cv2.INTER_AREA)

# create balls
balls = create_balls(nBall)

camera = cv2.VideoCapture(0)
start=True
# keep looping
while start:
	# grab the current frame
	(grabbed, frame) = camera.read()
	# resize the frame and flip it
	frame = imutils.resize(frame,width=width)
	frame = cv2.flip(frame, 1)
    # save the current frame
	current=frame.copy()
	if previous is None:
		previous =frame
    # find the absolute difference between current and previous frame
	diff=cv2.absdiff(current, previous)
	if initialDelay <= 0:
            # for each ball
    		for t in balls:
            		if t.active:
                            nzero = hit_value(diff, t)
                            x,y,_,_=t.getDimensions()
                            # find if nothing hit the ball
                            if  nzero<100: # under 100 pixels
                                # draw the ball in the frame
                                roi = frame[y:y+imgSize, x:x+imgSize]
                                img_bg = cv2.bitwise_and(roi, roi, mask=ball_mask_inv)
                                img_fg = cv2.bitwise_and(ball, ball, mask=ball_mask)
                                dst = cv2.add(img_bg, img_fg)
                                frame[y:y + imgSize, x:x + imgSize] = dst
                                # update the ball position
                                t.update()
                        		# if the ball hits the bottom
                                if t.y + imgSize >= frame.shape[0]:
                                		t.active = False
                                		nFall+=1
                                		# print("fall")
                                        # cv2.putText(frame, str("fall!"), (170, 190), cv2.FONT_HERSHEY_COMPLEX_SMALL, 2, (51, 0, 153), 3, cv2.LINE_AA)
                            # find if something hit the ball
                            else:
                                 # update its coordinate an speed
                                 t.x = random.randint(0,width-imgSize)
                                 t.y =0
                                 if t.speed[1] < 15:
                                     t.speed = (0, t.speed[1]+1)
                                 score += 1
    # save the current frame as previous
	previous=current.copy()
	initialDelay -=1
	# put score
	if nFall==5:
        	cv2.putText(frame, str("Game Over!"), (170, 190), cv2.FONT_HERSHEY_COMPLEX_SMALL, 2, (51, 0, 153), 3, cv2.LINE_AA)
	        cv2.putText(frame, str("Click q to quit!"), (170, 230), cv2.FONT_HERSHEY_COMPLEX_SMALL, 2, (51, 0, 153),1, cv2.LINE_AA)
        	cv2.putText(frame, str("Click r to restart!"), (170, 260), cv2.FONT_HERSHEY_COMPLEX_SMALL, 2, (51, 0, 153),1, cv2.LINE_AA)
	cv2.putText(frame, str("Score - "+str(score)), (20, 20), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 200, 0), 2, cv2.LINE_AA)
	cv2.putText(frame, str("Balls - "+str(nFall)+"/5"), (20,40), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 200, 0), 1, cv2.LINE_AA)

    # display frame
	# cv2.imshow("frame", frame)
	cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)
	cv2.setWindowProperty("window",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
	cv2.imshow("window", frame)
	key = cv2.waitKey(1) & 0xFF
	if key == ord("r"):
		initialDelay=0
		score=0
		for t in balls:
			t.y=0
			t.active=True
			nFall=0
	# if the 'q' key is pressed, stop the loop
	if key == ord("q"):
		break


# cleanup the camera and close any open windows
camera.release()
cv2.destroyAllWindows()
