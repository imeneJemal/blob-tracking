import cv2
import numpy as np

right = cv2.VideoCapture(2)
left = cv2.VideoCapture(1)
i=0
while(True):
    if not (left.grab() and right.grab()):
        print("No more frames")
        break
    _, rightFrame = right.retrieve()
    _, leftFrame = left.retrieve()
    nameR='t/r'+str(i)+'.jpg'
    nameL='t/l'+str(i)+'.jpg'

    cv2.imshow("leftFrame",leftFrame)
    cv2.imshow("rightFrame",rightFrame)


    if cv2.waitKey(1) & 0xFF == ord('c'):
        cv2.imwrite(nameR, rightFrame)
        cv2.imwrite(nameL, leftFrame)
        i += 1
left.release()
right.release()
cv2.destroyAllWindows()
