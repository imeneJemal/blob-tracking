import cv2
import numpy as np
from ball_detection import BallDetection
from disparity_map import DisparityMap

if __name__ == '__main__':
    left = cv2.VideoCapture(2)
    right = cv2.VideoCapture(1)
    cache = np.load("sterocaliboutput.npz")
    left_mapX=cache["left_mapX"]
    left_mapY=cache["left_mapY"]
    right_mapX=cache["right_mapX"]
    right_mapY=cache["right_mapY"]
    left_mtx=cache["left_mtx"]
    right_mtx=cache["right_mtx"]
    Q=cache["dispartity_to_depth"]
    focal_length=(left_mtx[0][0])
    base_line=6.4 # in cm
    triangulation_constant=focal_length*base_line
    bd=BallDetection()
    dm=DisparityMap()
    bg_top=100
    bg_left=300
    bg_rigth=350
    bg_buttom=150
    bg=True

    while(True):
        if not (left.grab() and right.grab()):
            print("No more frames")
            break
        _, left_frame = left.read() # # TODO: test .retrieve() function
        _, right_frame= right.read()
        left_frame=cv2.remap(left_frame,left_mapX,left_mapY,cv2.INTER_LINEAR)
        right_frame=cv2.remap(right_frame,right_mapX,right_mapY,cv2.INTER_LINEAR)
        # left_frame = cv2.pyrDown(left_frame) # downscale images for faster processing
        # right_frame = cv2.pyrDown(right_frame)
        # cv2.resize(left_frame, None,fx=0.5,fy=0.5); # resize images for faster processing
        # cv2.resize(right_frame, None,fx=0.5,fy=0.5);
        disparity=dm.get_diparity_map(left_frame,right_frame)
        cv2.imshow('disparity', disparity)
        # color_disparity=cv2.applyColorMap(disparity, cv2.COLORMAP_JET)
        center=bd.ball_detection(left_frame)
        points_3D = cv2.reprojectImageTo3D(disparity, Q)
        if bg:
            A_x=points_3D[(bg_left,bg_top)][0]
            A_y=points_3D[(bg_left,bg_top)][1]
            A_z=points_3D[(bg_left,bg_top)][2]
            D_x=points_3D[(bg_rigth,bg_buttom)][0]
            D_y=points_3D[(bg_rigth,bg_buttom)][1]
            D_z=points_3D[(bg_rigth,bg_buttom)][2]
            bg_depth=triangulation_constant/disparity[(bg_top,bg_left)]
        if center!= None and center[0]<points_3D.shape[0] and center[1]<points_3D.shape[1]:
            x= points_3D[center][0]
            y= points_3D[center][1]
            z= points_3D[center][2]
            depth=triangulation_constant/disparity[center]
            print("Center " ,center,"  Disparity ",disparity[center],
            " Depth ", format(depth, '.2f'),
            " X",  format(points_3D[center][0],'.1f'),
            " Y",  format(points_3D[center][1],'.1f'),
            " Z", format(points_3D[center][2],'.1f'))
            cv2.putText(left_frame,"bg:"+str(bg_depth)+"  obj:"+ str(depth),(50, 450) , cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 200, 0), 2, cv2.LINE_AA)

            state=True
            for neighbors in points_3D[center[0]:center[0]+5,center[1]:center[1]+5]:
                for neighbor in neighbors:
                    x=neighbor[0]
                    y=neighbor[1]
                    z=neighbor[2]
                    # if x>A_x or x<D_x or y>A_y or y<D_y or z!=A_z or z!=D_z :
                    if depth!=triangulation_constant/disparity[(bg_top,bg_left)]:
                        state=False
            if state:
                print("touch")
        bg=False
        cv2.rectangle(left_frame,(bg_left,bg_top),(bg_rigth,bg_buttom),(0,255,0),3)
        cv2.imshow('right_frame', right_frame)
        cv2.imshow('left_frame', left_frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    left.release()
    right.release()
    cv2.destroyAllWindows()
