## Computer Vision Project: Ball touching surface detection
Computer vision project using a pair of low cost webcams to detect a colored object, calculate its depth in real time and detect touch contact with a surface.
The code was developed in python using openCV3.
## Method
To obtain the distance we followed this steps: camera calibration, disparity calculation, object detection and depth calculation.
These are explained as follows:
1. Camera calibration:
The camera calibration is an important step in computer vision to get highly accurate representation of the real world in the captured images by correcting the lens distortion[1].
We need to take 20-30 pictures of a chessboard in different positions and angles to calibrate cameras and get camera parameters( matrix, distortion coefficients, rotation and translation vectors..).
In the stereo vision, we have to calibrate each camera individually then calibrate both cameras together using stereo calibration[2].
Each calibration returns the root mean square (RMS) re-projection error, usually it should be between 0.1 and 1.0 pixels in a good calibration[3].
Use calibration results to rectify the frames(remap).
In this example we get:
RMS_camera1=0.19
RMS_camera2=0.20
RMS_camera1=0.81
Some tips to get a good calibration[4].
2. Disparity calculation: we used opencv SGBM algorithm and wls filter[5] (other methods:BM algo, SGBM algo).
3. Object detection: using color, in this example we detect blue objects[6].
4. Depth calculation: tow methods are implemented to calculate the depth:
* equation: depth=base_line*focal_length/disparity
  base_line: distance between camera in cm
  focal_length: from the camera calibration in pixels
  disparity: in pixels[7].
  For the depth range calculation[8].
* openCV 3D reconstituation: [9].
The touch contact detection algorithm is simple:we examinate 5*5 neighborhood around the center,if all the pixels have the same distance as th background (green box) we consider this as contact [10].
## Source Code Overview
* capture_frame.py: deals with the capturing of frames to take pictures of the chessboard. Press 'c' character to capture a frame.  
* camera_calibrate.py: Take an argument (directory path where the pictures are saved).
* ball_detection.py: This consists of the function that detect the object it take as argument a frame and return the center of the detected object.
* main.py : This is the main function: it take frames from tow camera connected by USB and return the object depth.it consider
  Note: If left is not left or right is not right, either swap the USB connections or swap the 0 and 1 in the function VideoCapture().
## References
[1] https://docs.opencv.org/3.1.0/dc/dbb/tutorial_py_calibration.html
[2] https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html#stereocalibrate
https://github.com/bvnayak/stereo_calibration
[3] https://stackoverflow.com/questions/29628445/meaning-of-the-retval-return-value-in-cv2-calibratecamera
[4] https://stackoverflow.com/questions/12794876/how-to-verify-the-correctness-of-calibration-of-a-webcam/12821056#12821056
[5] http://timosam.com/python_opencv_depthimage
[6] https://www.pyimagesearch.com/2015/09/14/ball-tracking-with-opencv/
[7] https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_calib3d/py_depthmap/py_depthmap.html
[8] http://answers.opencv.org/question/175620/stereo-depth-range-calculation/
[9] https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html?highlight=calib#reprojectimageto3d
[10] https://static1.squarespace.com/static/564b5107e4b087849452ea4b/t/591c69621b10e37f47ec2354/1495034217345/direct.pdf , Touch Contact Detection
### Useful code
https://github.com/mkokshoorn/Stereo-Webcam-Depth-Detection#stereo-webcam-depth-detection
https://github.com/rachillesf/stereoMagic
## Issues
* Inaccurate depth calculation due to:
-The cameras have different focal focal length.
-There is no sensor size and field of view(vof) infromation to calculate the depth range.
