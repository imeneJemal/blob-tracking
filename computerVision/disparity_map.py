import cv2
import numpy as np

class DisparityMap:
    def __init__(self):
         # SGBM Parameters
        self.window_size = 3 # wsize default 3; 5; 7 for SGBM reduced size image; 15 for SGBM full size image (1300px and above); 5 Works nicely
        self.left_matcher = cv2.StereoSGBM_create(
            minDisparity=0,
            numDisparities=128,             # max_disp has to be dividable by 16
            blockSize=5,                  # Matched block siz 3..11
            P1=8  *3* self.window_size ** 2,   # disparity smoothness
            P2=32 *3* self.window_size ** 2,  # disparity smoothness
            disp12MaxDiff=1,               # Maximum allowed difference  (non-positive value to disable the check)
            uniquenessRatio=15,            # 5-15
            speckleWindowSize=0,           #0 to disable speckle filtering or 50-200
            speckleRange=2,                #Maximum disparity variation within each connected component (1 or 2 )
            preFilterCap=63,
            mode=cv2.STEREO_SGBM_MODE_SGBM_3WAY
        )
        self.right_matcher = cv2.ximgproc.createRightMatcher(self.left_matcher)
        # FILTER Parameters
        self.lmbda = 80000
        self.sigma = 1.2
        self.wls_filter = cv2.ximgproc.createDisparityWLSFilter(matcher_left=self.left_matcher)
        self.wls_filter.setLambda(self.lmbda)
        self.wls_filter.setSigmaColor(self.sigma)

    def get_diparity_map(self,left_frame,right_frame):
        displ = self.left_matcher.compute(left_frame,right_frame).astype(np.float32)/16
        dispr = self.right_matcher.compute(right_frame,left_frame).astype(np.float32)/16
        displ = np.int16(displ)
        dispr = np.int16(dispr)
        filtered_img = self.wls_filter.filter(displ, left_frame, None, dispr)  # important to put "imgL" here!!!
        filtered_img_test=filtered_img
        filtered_img = cv2.normalize(src=filtered_img, dst=filtered_img, beta=0, alpha=255, norm_type=cv2.NORM_MINMAX);
        filtered_img = np.uint8(filtered_img)
        return filtered_img
