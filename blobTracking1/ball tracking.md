* Description: In this example we are going to track a ball. We are going to
  detect it using HSV color detection, then we are going to track it using the
  dense method.

* Definitions:
  -HSV(Hue-Saturation-Value 'brightness'/Tinte-Saturation-Valeur 'luminosité'):
   is a color space like RGV, but it seperate the color from the intensity.
   Hue: 0-360 degree (Red:0-60, Yellow:60-120.. Magenta	300-360)
   Saturation:(the amount of gray in the color) 0 to 100 percent
   [0 is gray,1 is the primary color]
   Value: 0-1 [0 is black]
   It is widely used because:
   -It support lighting changes while separate color components from intensity
   -It is simple to convert between RGB and HSV
  -HSV for hand segmentation: In our hand we can see different color variation,
   but the hue don't vary much,so hue value can be useful in hand segmentation.
   
* Algorithm:
  -Read, resize(to minimize processed area),flip(avoid mirror view),blur
  the image from the video.
  -Convert the image to HSV.
  -Detect a range of color from the image.
  -Apply a series of dilations and erosions to remove small blobs.
  -Find the maximum contour.
  -Get the minimum Enclosing Circle for the contour(center coordinates
  and radius)and the centroid of contour.
  -Display result

* Rq:
  -deque: a list-like data structure with super fast appends and pops
  -finding contour in OpenCv :is like finding white object from black background
  -resizing the frame  allows us to process the frame faster

----------
Refrences:
https://www.pyimagesearch.com/2015/09/14/ball-tracking-with-opencv/
https://www.lifewire.com/what-is-hsv-in-design-1078068
